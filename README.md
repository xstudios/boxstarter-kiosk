# Boxstarter Kiosk Machine

## Overview
A collection of Powershell scripts to quickly get a new kiosk machine up and running.

This script automatically configures a PC for "kiosk" mode. Saves time and keystrokes.

## Features
- Boot and Shutdown
    - Auto-End Tasks immediately
    - Auto-Kill hung applications immediately
    - Auto-Kill hung services immediately
- Explorer
    - Show extensions of known file-types
    - Show hidden files and folders
- Performance
    - Disable Info Tips on Files and Folders
    - Disable Optimize harddisk when idle
    - Disable paging of kernel and core-os
    - Power Management - Always On
- Start Menu
    - Delay in Start Menu
    - Automatically Sort Start Menu
    - Remove Help and Support
- Taskbar
    - Enable Balloon Tips
    - Enable Group similar Taskbar buttons
    - Disable Hide inactive icons
    - Disable Language-Bar
    - Disable Windows Tour popup
    - Hide Volume Control Icon in System Tray
    - Disable Security Notifications
- Desktop
    - Hide Internet Explorer Icon
    - Hide Recycle Bin Icon
    - Hide My Documents
- Touch Feedback
    - Disable On Screen Keyboard
    - Remove visual touch feedback
- Disable Screensaver
- Disable Black Viper Recommended Services
- Disable Services
    - Adobe Acrobat Update Service
    - Adobe Flash Player Update Service
    - Computer Browser
    - Network Access Protection Agen
    - Print Spooler
    - Remote Registry
    - Smart Card
    - Windows Image Acquisition (WIA)
    - Windows Search
    - Windows Update
- Disable Tablet PC Components
- Reboot Nightly at 3:00AM

## Usage
Open Powershell as an Administrator.
```powershell
# Download and unzip this repository
Invoke-WebRequest -Uri "https://bitbucket.org/xstudios/boxstarter-kiosk/get/master.zip" -OutFile "~/Downloads/boxstarter-kiosk.zip"
Expand-Archive -Path "~/Downloads/boxstarter-kiosk.zip" -DestinationPath "~/Downloads/" -Force
cd ~/Downloads/xstudios-boxstarter-kiosk-*

Set-ExecutionPolicy Unrestricted
dir *.ps1 | Unblock-File

.\install-boxstarter.ps1
```

Open **Boxstarter Shell** (now on your desktop) execute the following commands:
```powershell
cd ~/Downloads/xstudios-boxstarter-kiosk-*
.\install.ps1
```

> NOTE: The Boxtarter Shell icon is on your desktop. It looks like a box. Go figure.

## Install Xapp (Don't Forget This Step!)
Close and reopen Powershell as an Administrator. CD into the `boxstarter-kiosk` directory and execute the following command:
```powershell
.\install-xapp.ps1
```
