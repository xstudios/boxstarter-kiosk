@Echo off

:: EXAMPLE BOILERPLATE FILE ONLY! Customize to fit your needs.
:: This is the main entry point of the app and runs via a scheduled task at startup.

:: Kill Explorer - ensures we don't end up with a Windows taskbar over our app
TASKKILL /F /IM "explorer.exe"

:: Start python script (optional)
:: START /MIN "Manifest Ingest" "C:\Python39\Scripts\manifest-ingest.exe" --config=~/projname/config/manifest-ingest.cfg

timeout /t 3

:: Start the main application fullscreen at the desired resolution
"%UserProfile%\ProjectName\AppName\AppName.exe" -screen-fullscreen 1 -screen-width 1920 -screen-height 1080

:: Kill the Python script
:: TASKKILL /FI "WINDOWTITLE eq Manifest Ingest"

:: Auto start explorer on app exit
:: START "explorer.exe"

Exit
