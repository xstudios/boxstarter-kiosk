﻿Set-ExecutionPolicy Unrestricted
dir *.ps1 | Unblock-File

. { iwr -useb http://boxstarter.org/bootstrapper.ps1 } | iex; get-boxstarter -Force


$confirmation = Read-Host "Enable local Nexus repo (for use at X Studios only)? Are you sure?"
if ($confirmation -eq 'y') {
    $ipAddress = Read-Host "Please enter the IP address for the Nexus repository"
    Write-Output "Enable local Nexus repository..."
    # choco source disable -n=chocolatey
    choco source add -n=nexus -s=http://$ipAddress:8081/repository/chocolatey-group/ --priority=1
}
