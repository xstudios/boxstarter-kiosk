# $ErrorActionPreference = 'SilentlyContinue'

Write-Output "--------------------------------------------------------------------------------"
Write-Output "Setting specific services to Disabled..."

$services = @(
    "tzautoupdate" # Auto Time Zone Updater
    "MsKeyboardFilter" # Microsoft Keyboard Filter **
    "NetTcpPortSharing" # Net.Tcp Port Sharing Service **
    "RemoteRegistry" # Remote Registry
    "RemoteAccess" # Routing and Remote Access
    "shpamsvc" # Shared PC Account Manager
    "SCardSvr" # Smart Card
)

foreach ($service in $services) {
    # Write-Output "Trying to disable $service"
    # Get-Service -Name $service | Set-Service -StartupType Disabled

    If (Get-Service $service -ErrorAction SilentlyContinue) {
        If ((Get-Service $service).Status -eq 'Running') {
            Stop-Service $service -Force -ErrorAction SilentlyContinue
            Write-Host "Stopping $service"
            Get-Service -Name $service | Set-Service -StartupType Disabled
        }
        # Else {
        #     Write-Warning "$service found, but it is not running."
        # }
    } Else {
        Write-Warning "$service not found"
    }
}


Write-Output "--------------------------------------------------------------------------------"
Write-Output "Setting specific services to Manual..."

$manual_services = @(
    "AxInstSV" # ActiveX Installer (AxInstSV)
    "AJRouter" # AllJoyn Router Service
    "AppReadiness" # App Readiness
    "AppIDSvc" # Application Identity
    "Appinfo" # Application Information
    "ALG" # Application Layer Gateway Service
    "AppXSVC" # AppX Deployment Service (AppXSVC)
    "BthAvctpSvc" # AVCTP service
    "BITS" # Background Intelligent Transfer Service
    "BDESVC" # BitLocker Drive Encryption Service
    "wbengine" # Block Level Backup Engine Service
    "BTAGService" # Bluetooth Audio Gateway Service
    "bthserv" # Bluetooth Support Service
    "BluetoothUserService_?????" # Bluetooth User Support Service_?????
    "camsvc" # Capability Access Manager Service
    "CertPropSvc" # Certificate Propagation
    "ClipSVC" # Client License Service (ClipSVC)
    "KeyIso" # CNG Key Isolation
    "COMSysApp" # COM+ System Application
    "Browser" # Computer Browser
    "PimIndexMaintenanceSvc_?????" # Contact Data_?????
    "VaultSvc" # Credential Manager
    "DsSvc" # Data Sharing Service
    "DeviceAssociationService" # Device Association Service
    "DeviceInstall" # Device Install Service
    "DmEnrollmentSvc" # Device Management Enrollment Service
    "DsmSVC" # Device Setup Manager
    "DevicePickerUserSvc_?????" # DevicePicker_?????
    "DevicesFlowUserSvc_?????" # DevicesFlow_?????
    "DevQueryBroker" # DevQuery Background Discovery Broker
    "diagsvc" # Diagnostic Execution Service
    "WdiServiceHost" # Diagnostic Service Host
    "WdiSystemHost" # Diagnostic System Host
    "MSDTC" # Distributed Transaction Coordinator
    "dmwappushsvc" # dmwappushsvc
    "embeddedmode" # Embedded Mode
    "EFS" # Encrypting File System (EFS)
    "EntAppSvc" # Enterprise App Management Service
    "EapHost" # Extensible Authentication Protocol Service
    "Fax" # Fax **
    "fhsvc" # File History Service
    "fdPHost" # Function Discovery Provider Host
    "FDResPub" # Function Discovery Resource Publication
    "BcastDVRUserService_?????" # GameDVR and Broadcast User Service_?????
    "lfsvc" # Geolocation Service
    "GraphicsPerfSvc" # GraphicsPerfSvc
    "hidserv" # Human Interface Device Service
    "HvHost" # HV Host Service
    "vmickvpexchange" # Hyper-V Data Exchange Service
    "vmicguestinterface" # Hyper-V Guest Service Interface
    "vmicshutdown" # Hyper-V Guest Shutdown Service
    "vmicheartbeat" # Hyper-V Heartbeat Service
    "vmicvmsession" # Hyper-V PowerShell Direct Service
    "vmicrdv" # Hyper-V Remote Desktop Virtualization Service
    "vmictimesync" # Hyper-V Time Synchronization Service
    "vmicvss" # Hyper-V Volume Shadow Copy Requestor
    "IKEEXT" # IKE and AuthIP IPsec Keying Modules
    "irmon" # Infrared monitor service
    "UI0Detect" # Interactive Services Detection
    "SharedAccess" # Internet Connection Sharing (ICS)
    "IpxlatCfgSvc" # IP Translation Configuration Service
    "PolicyAgent" # IPsec Policy Agent
    "KtmRm" # KtmRm for Distributed Transaction Coordinator
    "lltdsvc" # Link-Layer Topology Discovery Mapper
    "wlpasvc" # Local Profile Assistant Service
    "LxssManager" # LxssManager **
    "MessagingService_?????" # MessagingService_?????
    "diagnosticshub.standardcollector.service" # Microsoft (R) Diagnostics Hub Standard Collector Service
    "wlidsvc" # Microsoft Account Sign-in Assistant
    "MSiSCSI" # Microsoft iSCSI Initiator Service
    "NgcSvc" # Microsoft Passport
    "NgcCtnrSvc" # Microsoft Passport Container
    "swprv" # Microsoft Software Shadow Copy Provider
    "smphost" # Microsoft Storage Spaces SMP
    "InstallService" # Microsoft Store Install Service
    "SmsRouter" # Microsoft Windows SMS Router Service
    "NaturalAuthentication" # Natural Authentication
    "NetTcpPortSharing" # Net.Tcp Port Sharing Service **
    "Netlogon" # Netlogon
    "NcdAutoSetup" # Network Connected Devices Auto-Setup
    "NcbService" # Network Connection Broker
    "Netman" # Network Connections
    "NcaSVC" # Network Connectivity Assistant
    "netprofm" # Network List Service
    "NetSetupSvc" # Network Setup Service
    "ssh-agent" # OpenSSH Authentication Agent
    "defragsvc" # Optimize Drives
    "WpcMonSvc" # Parental Controls
    "SEMgrSvc" # Payments and NFC/SE Manager
    "PNRPsvc" # Peer Name Resolution Protocol
    "p2psvc" # Peer Networking Grouping
    "p2pimsvc" # Peer Networking Identity Manager
    "PerfHost" # Performance Counter DLL Host
    "pla" # Performance Logs & Alerts
    "PhoneSvc" # Phone Service
    "PlugPlay" # Plug and Play
    "PNRPAutoReg" # PNRP Machine Name Publication Service
    "WPDBusEnum" # Portable Device Enumerator Service
    "PrintNotify" # Printer Extensions and Notifications
    "PrintWorkflowUserSvc_?????" # PrintWorkflow_?????
    "wercplsupport" # Problem Reports and Solutions Control Panel Support
    "QWAVE" # Quality Windows Audio Video Experience
    "RmSvc" # Radio Management Service
    "RasAuto" # Remote Access Auto Connection Manager
    "RasMan" # Remote Access Connection Manager
    "SessionEnv" # Remote Desktop Configuration
    "TermService" # Remote Desktop Services
    "UmRdpService" # Remote Desktop Services UserMode Port Redirector
    "RpcLocator" # Remote Procedure Call (RPC) Locator
    "RetailDemo" # Retail Demo Service
    "seclogon" # Secondary Logon
    "SstpSvc" # Secure Socket Tunneling Protocol Service
    "SensorDataService" # Sensor Data Service
    "SensrSvc" # Sensor Monitoring Service
    "SensorService" # Sensor Service
    "ScDeviceEnum" # Smart Card Device Enumeration Service
    "SCPolicySvc" # Smart Card Removal Policy
    "SNMPTRAP" # SNMP Trap
    "SharedRealitySvc" # Spatial Data Service
    "svsvc" # Spot Verifier
    "SSDPSRV" # SSDP Discovery
    "StateRepository" # State Repository Service
    "WiaRpc" # Still Image Acquisition Events
    "StorSvc" # Storage Service
    "TieringEngineService" # Storage Tiers Management
    "lmhosts" # TCP/IP NetBIOS Helper
    "TapiSrv" # Telephony
    "TimeBroker" # Time Broker
    "TabletInputService" # Touch Keyboard and Handwriting Panel Service
    "UsoSvc" # Update Orchestrator Service for Windows Update
    "upnphost" # UPnP Device Host
    "UserDataSvc_?????" # User Data Access_?????
    "UnistoreSvc_?????" # User Data Storage_?????
    "vds" # Virtual Disk
    "VSS" # Volume Shadow Copy
    "WalletService" # WalletService
    "WMSVC" # Web Management Service **
    "nan" # WarpJITSvc
    "TokenBroker" # Web Account Manager
    "WebClient" # WebClient
    "WFDSConSvc" # Wi-Fi Direct Services Connection Manager Service
    "SDRSVC" # Windows Backup
    "WbioSrvc" # Windows Biometric Service
    "FrameServer" # Windows Camera Frame Server
    "wcncsvc" # Windows Connect Now - Config Registrar
    "WdNisSvc" # Windows Defender Antivirus Network Inspection Service
    "WEPHOSTSVC" # Windows Encryption Provider Host Service
    "WerSvc" # Windows Error Reporting Service
    "Wecsvc" # Windows Event Collector
    "StiSvc" # Windows Image Acquisition (WIA)
    "wisvc" # Windows Insider Service
    "msiserver" # Windows Installer
    "LicenseManager" # Windows License Manager Service
    "WMPNetworkSvc" # Windows Media Player Network Sharing Service **
    "icssvc" # Windows Mobile Hotspot Service
    "TrustedInstaller" # Windows Modules Installer
    "spectrum" # Windows Perception Service
    "FontCache3.0.0.0" # Windows Presentation Foundation Font Cache 3.0.0.0 **
    "WAS" # Windows Process Activation Service **
    "PushToInstall" # Windows PushToInstall Service
    "WinRM" # Windows Remote Management (WS-Management)
    "W32Time" # Windows Time
    "wuauserv" # Windows Update
    "WaaSMedicSvc" # Windows Update Medic Service
    "WinHttpAutoProxySvc" # WinHTTP Web Proxy Auto-Discovery Service
    "dot3svc" # Wired AutoConfig
    "wmiApSrv" # WMI Performance Adapter
    "workfolderssvc" # Work Folders **
    "WwanSvc" # WWAN AutoConfig
    "XboxGipSvc" # Xbox Accessory Management Service
    "xbgm" # Xbox Game Monitoring
    "XblAuthManager" # Xbox Live Auth Manager
    "XblGameSave" # Xbox Live Game Save
    "XboxNetApiSvc" # Xbox Live Networking Service
)

foreach ($service in $manual_services) {
    #Write-Output "Trying to manual $service"
    # Get-Service -Name $service | Set-Service -StartupType Manual

    If (Get-Service $service -ErrorAction SilentlyContinue) {
        # If ((Get-Service $service).Status -eq 'Running') {
            Stop-Service $service -Force -ErrorAction SilentlyContinue
            Write-Host "Stopping $service"
            Get-Service -Name $service | Set-Service -StartupType Manual -ErrorAction SilentlyContinue
        # }
        # Else {
        #     Write-Warning "$service found, but it is not running."
        # }
    } Else {
        Write-Warning "$service not found"
    }
}

Write-Output "--------------------------------------------------------------------------------"
Write-Output "Setting specific services to Automatic..."

$manual_services = @(
    "BITS" # Background Intelligent Transfer Service
    "BrokerInfrastructure" # Background Tasks Infrastructure Service
    "BFE" # Base Filtering Engine
    "EventSystem" # COM+ Event System
    "CDPSvc" # Connected Device Platform Service
    "CDPUserSvc_?????" # Connected Devices Platform User Service_?????
    "DiagTrack" # Connected User Experiences and Telemetry
    "CoreMessagingRegistrar" # CoreMessaging
    "CryptSvc" # Cryptographic Services
    "DusmSvc" # Data Usage
    "DcomLaunch" # DCOM Server Process Launcher
    "DoSvc" # Delivery Optimization
    "Dhcp" # DHCP Client
    "DPS" # Diagnostic Policy Service
    "TrkWks" # Distributed Link Tracking Client
    "Dnscache" # DNS Client
    "MapsBroker" # Downloaded Maps Manager
    "gpsvc" # Group Policy Client
    "IISADMIN" # IIS Admin Service **
    "iphlpsvc" # IP Helper
    "LSM" # Local Session Manager
    "LPDSVC" # LPD Service **
    "MSMQ" # Message Queuing **
    "MSMQTriggers" # Message Queuing Triggers **
    "ftpsvc" # Microsoft FTP Service **
    "NetMsmqActivator" # Net.Msmq Listener Adapter **
    "NetPipeActivator" # Net.Pipe Listener Adapter **
    "NetTcpActivator" # Net.Tcp Listener Adapter **
    "NlaSvc" # Network Location Awareness
    "nsi" # Network Store Interface Service
    "Power" # Power
    "Spooler" # Print Spooler
    "PcaSvc" # Program Compatibility Assistant Service
    "RpcSs" # Remote Procedure Call (RPC)
    "iprip" # RIP Listener **
    "RpcEptMapper" # RPC Endpoint Mapper
    "SamSs" # Security Accounts Manager
    "wscsvc" # Security Center
    "LanmanServer" # Server
    "ShellHWDetection" # Shell Hardware Detection
    "simptcp" # Simple TCP/IP Services **
    "SNMP" # SNMP Service **
    "sppsvc" # Software Protection
    "SysMain" # Superfetch
    "OneSyncSvc_?????" # Sync Host_?????
    "SENS" # System Event Notification Service
    "SystemEventsBroker" # System Events Broker
    "SgrmBroker" # System Guard Runtime Monitor Broker
    "Schedule" # Task Scheduler
    "Themes" # Themes
    "UserManager" # User Manager
    "ProfSvc" # User Profile Service
    "AudioSrv" # Windows Audio
    "AudioEndpointBuilder" # Windows Audio Endpoint Builder
    "Wcmsvc" # Windows Connection Manager
    "WinDefend" # Windows Defender Antivirus Service
    "MpsSvc" # Windows Defender Firewall
    "SecurityHealthService" # Windows Defender Security Center Service
    "EventLog" # Windows Event Log
    "FontCache" # Windows Font Cache Service
    "Winmgmt" # Windows Management Instrumentation
    "WpnService" # Windows Push Notifications System Service
    "WpnUserService_?????" # Windows Push Notifications User Service_?????
    "WSearch" # Windows Search
    "WlanSvc" # WLAN AutoConfig
    "LanmanWorkstation" # Workstation
    "W3SVC" # World Wide Web Publishing Service **
)

foreach ($service in $manual_services) {
    #Write-Output "Trying to manual $service"
    # Get-Service -Name $service | Set-Service -StartupType Manual

    If (Get-Service $service -ErrorAction SilentlyContinue) {
        # If ((Get-Service $service).Status -eq 'Stopped') {
            Start-Service $service
            Write-Host "Starting $service"
            Get-Service -Name $service | Set-Service -StartupType Automatic
        # }
        # Else {
        #     Write-Warning "$service found, but it is not running."
        # }
    } Else {
        Write-Warning "$service not found"
    }
}
