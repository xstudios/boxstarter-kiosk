Write-Output "--------------------------------------------------------------------------------"
Write-Output "Uninstall unecessary Windows features..."
# choco list --source windowsFeatures


$features = @(
    "IIS-WebServerRole"
    "IIS-WebServer"

    # "FaxServicesClientPackage"
    "Printing-Foundation-Features"
    "Printing-Foundation-InternetPrinting-Client"
    "Printing-PrintToPDFServices-Features"
    "Printing-XPSServices-Features"
    # "Print.Fax.Scan"

    "WorkFolders-Client"

    "SearchEngine-Client-Package"

    "SMB1Protocol"
    "SMB1Protocol-Client"
    "SMB1Protocol-Server"
    "SMB1Protocol-Deprecation"
)

foreach ($feature in $features) {
    Write-Output "Trying to remove $feature"
    choco uninstall $feature -source windowsfeatures --limitoutput
}

# Add/Remove Programs > Manage Optional Features
Get-WindowsCapability -online | ? {$_.Name -like '*ContactSupport*'} | Remove-WindowsCapability -online
Get-WindowsCapability -online | ? {$_.Name -like '*QuickAssist*'} | Remove-WindowsCapability -online
# Get-WindowsCapability -online | ? {$_.Name -like 'Print.Fax.Scan'} | Remove-WindowsCapability -online

# -----------------------------------------------------------------------------

# Write-Output "--------------------------------------------------------------------------------"
# Write-Output "Install some Windows features..."

# $features = @(
#    "OpenSSH.Server"
#    "OpenSSH.Client"
# )

# foreach ($feature in $features) {
#     Write-Output "Trying to install $feature"
#     choco install $feature -source windowsfeatures
# }
