# Check if the script is running as administrator
# if (-Not (New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
#     # If not, prompt for elevation
#     Start-Process PowerShell -ArgumentList "-File `"$PSCommandPath`"" -Verb RunAs
#     Exit
# }

Write-Output "--------------------------------------------------------------------------------"
Write-Output "WINDOWS UPDATE CONFIG"
Write-Output "--------------------------------------------------------------------------------"

# Disable notifications
# Automatic updates are enabled, but users are not notified when updates are downloaded and installed automatically.
# Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings" -Name UxOption -Type DWord -Value 1

Write-Output "Do NOT notify when a restart is required to finish updating"
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings" -Name RestartNotificationsAllowed2 -Type DWord -Value 0

Write-Output "Set active hours"
# Set active hours 6am-12am
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings" -Name ActiveHoursStart -Value 6 -Type DWORD -Force
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings" -Name ActiveHoursEnd -Value 0 -Type DWORD -Force

Write-Output "Extend Windows Update Pause Options"
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings" -Name FlightSettingsMaxPauseDays -Value 3647 -Type DWORD -Force

Write-Output "Pause Windows Updates"
# Get current timestamp in ISO 8601 format
$currentTime = Get-Date -Format s
$currentTime = $currentTime + "Z"
Write-Host "Now date: $currentTime"
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings" -Name PauseUpdatesStartTime -Value $currentTime -Type String -Force
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings" -Name PauseQualityUpdatesStartTime -Value $currentTime -Type String -Force
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings" -Name PauseFeatureUpdatesStartTime -Value $currentTime -Type String -Force

$currentDate = Get-Date
$futureDate = $currentDate.AddDays(3647)
$futureDateISO = $futureDate.ToString("yyyy-MM-ddTHH:mm:ss") + "Z"
Write-Host "Future date: $futureDateISO"
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings" -Name PauseUpdatesExpiryTime -Value $futureDateISO -Type String -Force
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings" -Name PauseQualityUpdatesEndTime -Value $futureDateISO -Type String -Force
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\WindowsUpdate\UX\Settings" -Name PauseFeatureUpdatesEndTime -Value $futureDateISO -Type String -Force

# Allow us to debug as standalone script
# Read-Host "Press Enter to continue..."