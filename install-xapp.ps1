# Run using elevated permissions (eg - Run as Admininstrator)

$myFile="xapp_monitor-0.3.14-py3-none-any.whl"

# Download the distribution zip
# $client = new-object System.Net.WebClient
# $client.DownloadFile("https://xstudios-pypi.s3.amazonaws.com/$myFile","$env:USERPROFILE\Downloads\$myFile")

# Install python requirements (assuming we have Python 3.x installed already)
Set-Location $env:USERPROFILE
python -m venv .venvs\xappmonitor_env
.\.venvs\xappmonitor_env\Scripts\Activate

# Install package
python -m pip install -U pip setuptools wheel
# pip install $env:USERPROFILE\Downloads\$myFile\
python -m pip install "https://xstudios-pypi.s3.amazonaws.com/$myFile"

# Cleanup after ourselves
# Remove-Item "$env:USERPROFILE\Downloads\$myFile"

# CD back into the location we are running script from
Set-Location $PSScriptRoot

# ------------------------------------------------------------------------------

Write-Output "Create a startup task for Xapp ..."

$taskName = "Start Xapp Monitor"

$exists = Get-ScheduledTask | Where-Object {$_.TaskName -like $taskName}
if ($exists) {
   Unregister-ScheduledTask -TaskName $taskName -Confirm:$false
}

$action = New-ScheduledTaskAction -Execute "$env:userprofile\.venvs\xappmonitor_env\Lib\site-packages\xapp_monitor\data\run-hidden.vbs"
$trigger = New-ScheduledTaskTrigger -AtStartup
$trigger.Delay = 'PT30S'
$principal = New-ScheduledTaskPrincipal -GroupId "BUILTIN\Administrators" -RunLevel Highest
Register-ScheduledTask -Action $action -Trigger $trigger -TaskName $taskName -Description $taskName -Principal $principal
