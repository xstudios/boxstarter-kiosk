# Boxstarter options
$Boxstarter.RebootOk=$true # Allow reboots?
$Boxstarter.NoPassword=$false # Is this a machine with no login password?
$Boxstarter.AutoLogin=$true # Save my password securely and auto-login after a reboot

Disable-MicrosoftUpdate
Update-ExecutionPolicy Unrestricted
# Set-WindowsExplorerOptions -EnableShowHiddenFilesFoldersDrives -EnableShowProtectedOSFiles -EnableShowFileExtensions -EnableShowFullPathInTitleBar -EnableOpenFileExplorerToQuickAccess -EnableShowRecentFilesInQuickAccess -EnableShowFrequentFoldersInQuickAccess -EnableExpandToOpenFolder -EnableShowRibbon -EnableItemCheckBox

# Set profile to private
Set-NetConnectionProfile -NetworkCategory "Private"

# Disable-UAC

# Rename the Computer
$computername = Read-Host "Computer name (eg - CPU-101)?"
if ($env:computername -ne $computername) {
   Rename-Computer -NewName $computername
}

# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Relaunch the script with administrator privileges
Function RequireAdmin {
    If (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]"Administrator")) {
        Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`" $PSCommandArgs" -WorkingDirectory $pwd -Verb RunAs
        Exit
    }
}

# Wait for key press
Function WaitForKey {
    Write-Output "`nPress any key to continue..."
    [Console]::ReadKey($true) | Out-Null
}

# Restart computer
Function Restart {
    Write-Output "Restarting..."
    Restart-Computer
}

# -----------------------------------------------------------------------------

# . ".\windows-updates.ps1"
. ".\windows-update-config.ps1"
. ".\remove-default-apps.ps1"
# . ".\remove-windows-features.ps1"
. ".\remove-bloatware.ps1"
. ".\firewall-config.ps1"
. ".\disable-notification-center.ps1"
. ".\windows-config.ps1"
. ".\install-apps.ps1"
# . ".\disable-services.ps1"
. ".\reboot-daily-task.ps1"
. ".\create-task-startup.ps1"
. ".\install-teamviewer.ps1"

# Enable-UAC

# if (Test-PendingReboot) { Invoke-Reboot }
$tweaks = @(
    ### Require administrator privileges ###
    # "RequireAdmin",

    # "DisableSMBServer"

    #Auxiliary Functions
    "WaitForKey"
    "Restart"
)

# Call the desired tweak functions
$tweaks | ForEach { Invoke-Expression $_ }
