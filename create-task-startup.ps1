# -----------------------------------------------------------------------------
# STARTUP TASK
# -----------------------------------------------------------------------------

# Copy our example file to the user directory
if (!(Test-Path "$env:USERPROFILE\Startup.bat")) {
    Write-Output "Copy Startup.bat to user directory..."
    Copy-Item "examples\Startup.bat" -Destination "$env:USERPROFILE"
}

Write-Output "Create a startup task ..."

$taskName = "Start App"

$exists = Get-ScheduledTask | Where-Object {$_.TaskName -like $taskName}
if ($exists) {
   Unregister-ScheduledTask -TaskName $taskName -Confirm:$false
}

$action = New-ScheduledTaskAction -Execute "$env:USERPROFILE\Startup.bat"
$trigger = New-ScheduledTaskTrigger -AtStartup
$trigger.Delay = 'PT30S'
Register-ScheduledTask -Action $action -Trigger $trigger -TaskName $taskName -Description $taskName
