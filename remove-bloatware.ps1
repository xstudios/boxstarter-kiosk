Write-Output "--------------------------------------------------------------------------------"
Write-Output "Uninstall bloatware applications..."
#get-wmiobject -class Win32Reg_AddRemovePrograms | Select Name
#get-wmiobject -class Win32_Product | Select Name


$apps = @(
    "*Maxx Audio Installer*"
    "Dell SupportAssist*"
    "Dell Digital Delivery"
    "Dell Command | Update"
    # "ExpressVPN"
)

foreach ($app in $apps) {
    Write-Output "Trying to remove $app ..."

    $app = Get-WmiObject -Class Win32_Product | Where-Object {$_.Name -like $app }
    if ($app) {
        $app.Uninstall()
    }
}
