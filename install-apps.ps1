Write-Output "--------------------------------------------------------------------------------"

# Apps
# Install a list of packages if present
Write-Output "Installing packages..."
choco feature enable -n allowGlobalConfirmation
choco install Microsoft-Windows-Subsystem-Linux -source windowsfeatures --limitoutput

# Dev tools
choco install PowerShell --limitoutput
choco install curl --limitoutput
choco install git -params '"/GitAndUnixToolsOnPath /WindowsTerminal"' -v --limitoutput
choco install python  # --version 3.12.3
choco install commandwindowhere --limitoutput
choco install microsoft-build-tools
choco install make
# choco install microsoft-windows-terminal
choco install nmap

# Apps
choco install sublimetext3 --limitoutput
choco install visualstudiocode --limitoutput
choco install googlechrome --limitoutput

# choco install nvidia-display-driver
