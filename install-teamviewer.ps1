# Download
$url = "https://download.teamviewer.com/download/version_15x/TeamViewer_Setup.exe"
$output = "$env:USERPROFILE\Downloads\TeamViewer_Setup.exe"
$start_time = Get-Date

if (!(Test-Path $output)) {
    $wc = New-Object System.Net.WebClient
    $wc.DownloadFile($url, $output)
    Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"
}


# Install
function IsInstalled( $program ) {
    $x86 = ((Get-ChildItem "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall") |
        Where-Object { $_.GetValue( "DisplayName" ) -like "*$program*" } ).Length -gt 0;
    $x64 = ((Get-ChildItem "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall") |
        Where-Object { $_.GetValue( "DisplayName" ) -like "*$program*" } ).Length -gt 0;
    return $x86 -or $x64;
}

if (IsInstalled("Teamviewer")) {
    # Do nothing - can't seem to do an !IsInstalled to avoid this
} else {
    # Install as Admin
    Start-Process -Filepath "$output" -Verb runAs
}
