# -----------------------------------------------------------------------------
# TURN ON FIREWALL & BLOCK POPUP NOTIFICATIONS
# -----------------------------------------------------------------------------

Write-Output "Turn on Firewall ..."

# Turn on Firewall (For some reason can interfere with Bluetooth and Steam)
Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled True

Write-Output "Turn off Firewall Popup Notifications ..."

# Turn off firewall notifications
netsh firewall set notifications mode = disable profile = all

# -----------------------------------------------------------------------------
# ADD FIREWALL RULES
# -----------------------------------------------------------------------------

Write-Output "Add Firewall rules ..."

# Allow pings
netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
netsh advfirewall firewall add rule name="ICMP Allow incoming V6 echo request" protocol=icmpv6:8,any dir=in action=allow

# MQTT
netsh advfirewall firewall add rule name="Open Port 1883" protocol=TCP localport=1883 dir=in action=allow


# https://dejulia489.github.io/2017-05-07-InstallingOpenSSHOnWindows10/
# SSH
# New-NetFirewallRule -Protocol TCP -LocalPort 22 -Direction Inbound -Action Allow -DisplayName SSH

# Set-Service SSHD -StartupType Automatic
# Set-Service SSH-Agent -StartupType Automatic
